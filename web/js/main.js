activate_terminal = function(selector) {
    selector.terminal("terminal", {
        login: false,
        greetings: "Welcome to goTrade"
    });
}