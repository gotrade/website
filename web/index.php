<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../src/views',
    'twig.options' => array('strict_variables' => false)
));

$app['debug'] = true;


$app->get('/demo', function() use ($app) {
    return $app['twig']->render('demo', array('page'=>'demo', 'pagejs' => "<script>activate_terminal($('.row'))</script>"));
});

$app->get('/about', function() use ($app) {
    return $app['twig']->render('about', array('page'=>'about'));
});

$app->get('/contact', function() use ($app) {
    return $app['twig']->render('contact', array('page'=>'contact'));
});

$app->post('/terminal', function() use ($app) {
	$code = json_decode($GLOBALS['HTTP_RAW_POST_DATA'], true);

	$request = $code['method'] . " " . implode(" ", $code['params']);
	
    return json_encode(array(
		"jsonrpc" => "2.0",
		'result' => $request,
		'id' => "2"));
});

$app->get('/', function() use ($app) {
    return $app['twig']->render('skeleton', array('page'=>'home', 'pagejs' => ""));
})->bind('main');
















$app->run();